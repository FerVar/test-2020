"""
Se declaro dos diferentes views para manejar el caso de la creación de los datos así como la obtención de elllos
por medio de una llave primaria.
"""
from rest_framework import status
from rest_framework.generics import CreateAPIView,  RetrieveAPIView
from rest_framework.response import Response
from .serializers import UserSerializer
from .models import MyUser


# view para crear un usuario o regresar si existe su información
class CreateUser(CreateAPIView):
    serializer_class = UserSerializer
    queryset = MyUser.objects.all()

    def create(self, request):  # sobrescribimos el método create
        serealizer = self.get_serializer(data=request.data)
        serealizer.is_valid(raise_exception=True)
        # se realizo la comprobación de usuario si vemos que el username existe
        if MyUser.objects.filter(username=self.request.data['username']).exists():
            name = MyUser.objects.get(username=self.request.data['username'])
            new_response = UserSerializer(name).data
            return Response({'message': 'Este usuario ya existe' + str(new_response)}, status=status.HTTP_226_IM_USED)
        serealizer.save()
        return Response(serealizer.data, status=status.HTTP_201_CREATED)


class RetrieveUser(RetrieveAPIView):  # view para obtener un usuario por pk
    serializer_class = UserSerializer
    queryset = MyUser.objects.all()

    def retrieve(self, request, pk=None):
        # obtenemos el user por medio de un filter
        user = MyUser.objects.filter(id=pk).first()

        if user:
            user_serializer = UserSerializer(user)
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        return Response({'message': 'Usuario no existente'}, status=status.HTTP_204_NO_CONTENT)
