from django.urls import path
from . import views


urlpatterns = [
    # url para crear usuario
    path('users/post', views.CreateUser.as_view(),),
    # url para regresar un usuario por pk
    path('users/retrive/<int:pk>', views.RetrieveUser.as_view())

]
