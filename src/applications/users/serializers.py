"""
Serializador utilizado en nuestras vistas CreteaUser y RetriveUser
al estar usando el modeleo User heredado en nuestro modelo MyUser determinamos los
fields que son importantes para nuestra apliacación.
"""
from django.contrib.auth.validators import UnicodeUsernameValidator
from rest_framework import serializers
from .models import MyUser


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = MyUser
        fields = (
            'id',
            'username',
            'password',
            'email',
            'first_name',
            'last_name',)
        extra_kwargs = {
            # Se utilizo los validators ya que al crear el serializer por medio de modelserializer se queria quitar
            # el validatorunique para manejar nosotros esa  validación
            'username': {'validators': [UnicodeUsernameValidator]},
        }

    def create(self, validated_data):
        return MyUser.objects.create_user(**validated_data)
